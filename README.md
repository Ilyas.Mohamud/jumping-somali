### Jumping Somali
Jumping somali is a 2d-platform game created where the player will jump to reach the goal at the top of the tower.<br />
Created by Ilyas Mohamud


### Setup

Install Maven <br />
Clone Git Repository: https://git.app.uib.no/Ilyas.Mohamud/jumping-somali
play the game by running src/main/java/Game/Main.java

### Play:
You win the game by going through two stages and getting the Treasure chest at the top of the second stage<br />
To pass the first stage you need to kill all enemies and get on the golden boat <br />
On stage one you need to find a boat at the top of the map, and it will take you to the final stage <br />


#### Controls:
The controls for the game are <br />
A = Move right <br />
D = Move Left <br />
SpaceBar = Jump (hold space for longer jump) <br />
F = To shoot <br />
ESC = Go back to MainMenu


### Known bugs:
if your game doesnt find the png/jpg files, change the textures to a direct path instead of the local path <br />

If you use the Plug-in "Korge", the IDE crashes if you open the tmx file, try to open them in file explorer instead
