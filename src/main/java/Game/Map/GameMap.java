package Game.Map;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import Game.Objects.*;

import java.util.ArrayList;

import static Game.Map.TileType.TILE_SIZE;


/**
 * This class is responsible for creating and maintaining the TiledGameMap
 * object based on the map file.
 * It makes it possible to identify the TileTypes based on location,
 * and get certain attributes of these tiles.
 */
public abstract class GameMap {

    protected ArrayList<Entity> entities;
    protected ArrayList<Enemy> enemies;
    protected ArrayList<Item> items;

    public Player player;
    public Enemy enemy1;
    public Enemy enemy2;
    public Enemy enemy3;

    /**
     * Initiates entities which will be used in the game. Creates a list for both
     * player and enemy which will make it easier to update them throughout the game.
     */
    public GameMap () {
        enemies = new ArrayList<>();
        entities = new ArrayList<>();
        items = new ArrayList<>();
        player = new Player(TILE_SIZE * 2, TILE_SIZE * 20, this);
        enemy1 = new Enemy(TILE_SIZE , TILE_SIZE * 26, this);
        enemy2 = new Enemy(TILE_SIZE * 15, TILE_SIZE * 14, this);
        enemy3 = new Enemy(TILE_SIZE * 9, TILE_SIZE * 29, this);
        entities.add(player);
        entities.add(enemy1);
        entities.add(enemy2);
        entities.add(enemy3);
        enemies.add(enemy1);
        enemies.add(enemy2);
        enemies.add(enemy3);
        items.add(new Treasure(TILE_SIZE *18, TILE_SIZE * 46, ItemType.TREASURECHEST));
        items.add(new Treasure(TILE_SIZE * 3, TILE_SIZE * 32 , ItemType.GOLDBOAT));
        items.add(new Weapon(TILE_SIZE * 4 , TILE_SIZE * 6 , ItemType.BULLET, 5f*TILE_SIZE, 10, 10*TILE_SIZE, 100));
    }

    /**
     * Sets the view for the camera, and renders the TiledGameMap.
     *
     * @param camera - the camera to show the chosen area of the map.
     */
    public void render(OrthographicCamera camera, SpriteBatch batch) {
        for (Item item : items) {
            item.render(batch);
        }
        for (Entity entity : entities) {
            entity.render(batch);
        }
    }

    /**
     * updates all entities in the entity array.
     *
     * @param delta - change in time since last update
     */
    public void update (float delta) {
        ArrayList<Entity> entitiesCopy = (ArrayList<Entity>) entities.clone();
        for (Entity entity : entitiesCopy) {
            entity.update(delta, -9.8f);
            if (!entity.isAlive()) {
                entities.remove(entity);
                if (entity.getName().equals("Enemy")) {
                    enemies.remove(entity);
                }
            }
        }
        for (Item item : items) {
            item.update(player, delta);
        }
    }
    public abstract void dispose ();

    /**
     *
     * @param layer - In which layer of the map is the tile located
     * @param x - x-position of the map
     * @param y - y-position of the map
     * @return The type of the tile found at location: (layer) (x, y)
     */
    public TileType getTileTypeByLocation(int layer, float x, float y) {
        return this.getTileTypeByCoordinate(layer, (int) (x / TILE_SIZE),
                (int) (y / TILE_SIZE));
    }

    /**
     * Gets the TileType object at a certain location (coordinate) on the map.
     *
     * @param layer - specifies the layer to get the TileType from.
     * @param col - the column where the TileType is located.
     * @param row - the row where the TileType is located.
     * @return - the TileType at chosen layer, col and row.
     */
    public abstract TileType getTileTypeByCoordinate (int layer, int col, int row);

    /**
     * Checks if a given rectangle is collides with a tile in map.
     *
     * @param x - x-position of the rectangle
     * @param y - y-position of the rectangle
     * @param width - width of the rectangle
     * @param height - height of the rectangle
     * @return - True if the rectangle collides with a tile. False otherwise.
     */
    public boolean rectCollidesWithMap(float x, float y, int width, int height){
        if (x < 0 || y < 0 || x + width > getPixelWidth() || y + height > getPixelHeight() ){
            return true;
        }
        for (int row = (int) (y / TILE_SIZE); row < Math.ceil((y + height) / TILE_SIZE); row++){
            for (int col = (int) (x / TILE_SIZE); col < Math.ceil((x + width) / TILE_SIZE); col++){
                for (int layer = 0; layer < getLayers(); layer++){
                    TileType type = getTileTypeByCoordinate(layer, col, row);
                    if (type != null && type.isCollidable()){
                        return true;
                    }
                }
            }
        }
        return false;
    }


    /**
     *
     * @return the width of the map
     */
    public abstract int getWidth ();

    /**
     *
     * @return the height of the map
     */
    public abstract int getHeight ();

    /**
     *
     * @return the layers of the map
     */
    public abstract int getLayers ();

    /**
     *
     * @return width of the map in pixels
     */
    public int getPixelWidth(){
        return this.getWidth() * TILE_SIZE;
    }

    /**
     *
     * @return the height of the map in pixels
     */
    public int getPixelHeight(){
        return this.getHeight() * TILE_SIZE;
    }

    /**
     *
     * @return returns the array of entities
     */
    public ArrayList getEntities() {
        return entities;
    }

    /**
     *
     * @return returns the player
     */
    public Player getPlayer() {
        return player;
    }

    /**
     *
     * @return returns the array of enemies
     */
    public ArrayList<Enemy> getEnemies() {
        ArrayList<Enemy> enemiesCopy = new ArrayList<Enemy>();
        for (Entity entity : entities) {
            if(entity instanceof Enemy) {
                enemiesCopy.add((Enemy)entity);
            }
        }
        return enemiesCopy;
    }

    /**
     *
     * @return returns the the first weapon found in the scene
     */
    public Weapon getWeapon() {
        for (Item item : items) {
            if(item instanceof Weapon) {
                return (Weapon)item;
            }
        }

        return null;

    }

    /**
     *
     * @return returns the first treasure found in the scene
     */
    public Treasure getTreasure() {
        for (Item item : items) {
            if(item instanceof Treasure) {
                return (Treasure)item;
            }
        }

        return null;

    }

    public String gameStatus() {
        if (!player.isAlive()) {
            return "Game over";
        } else if (enemies.isEmpty() && player.hasBoat()) {
            return "Stage complete";
        }

        return "Game not over";

    }
}

