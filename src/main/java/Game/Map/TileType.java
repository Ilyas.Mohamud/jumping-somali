package Game.Map;

import java.util.HashMap;

/**
 * Set of constant objects on the map
 * to keep track of what is, and isn't collidable
 */
public enum TileType {

    SAND(165, true, "Sand"),
    WOOD(139, true, "Wood"),
    GRASS_TALL(58, false, "Tall Grass"),
    GRASS_SHORT(44, false, "Short Grass"),
    COBBLESTONE(187, true, "Cobblestone"),
    LAVA(109, true, "Lava",5),
    PLANKS(193, true, "Planks"),
    WATER(163, false, "Water"),
    GREY_COBBLESTONE(195,true,"Grey cobblestone"),
    GOLD_WALL(33,true,"Gold wall"),
    GLASS(60,true,"Glass");




    public static final int TILE_SIZE = 32; //The size of a tile in pixels

    private int id; //id of the tile in the TMX file (game map)
    private final boolean collidable; //true if the tile is collidable. False otherwise
    private String name; // Name of the tile
    private float damage; //Damage given for entities in contact of the tile

    /**
     * Constructor for the tile
     * @param id
     * @param collidable
     * @param name
     */
    TileType(int id, boolean collidable, String name){
        this(id, collidable, name, 0);
    }

    /**
     * Constructor for the tiles that do damage
     * @param id
     * @param collidable
     * @param name
     */
    TileType(int id, boolean collidable, String name, float damage){
        this.id = id;
        this.collidable = collidable;
    }


    /**
     * Returns the identifier for the TileType
     *
     * @return TileType id
     */
    private Integer getId() {
        return id;
    }


    /**
     * HashMap made to easily to identify the TilType on a map
     */
    private static HashMap<Integer, TileType> tileMap;


    /**
     *Loops through every tile, and adds it to the HashMap tileMap
     */
    static {
        tileMap = new HashMap<>();
        for (TileType tileType : TileType.values()) {
            tileMap.put(tileType.getId(), tileType);
        }
    }


    /**
     *Returns the TileType by the id
     * @param id
     * @return TileType id
     */
    public static TileType getTileTypeById(int id) {
        return tileMap.get(id);
    }

    /**
     *
     * @return True if the given tile is collidable. False otherwise
     */
    public boolean isCollidable(){
        return this.collidable;
    }
}

