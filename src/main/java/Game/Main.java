package Game;

import Game.Screens.MyGame;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;

public class Main {
    /**
     * Game Launcher
     */
    public static void main(String[] args) {
        Lwjgl3ApplicationConfiguration cfg = new Lwjgl3ApplicationConfiguration();
        cfg.setTitle("Jumping-Somali");
        cfg.setWindowedMode(1920, 1080);
        new Lwjgl3Application(new MyGame(), cfg);
    }
}