package Game.Screens;

import Game.Map.GameMap;
import Game.Map.TiledGameMap;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;


public class PlayScreen implements Screen {
    private SpriteBatch batch;
    private OrthographicCamera camera;
    public GameMap gameMap;
    float deltaX, deltaY;
    MyGame Handler;
    private String extraMap = "LastMap.tmx";

    /**
     * Constructor for PlayScreen
     * Creates a new camera and gamemap
     * @param Handler
     * @param map
     */
    public PlayScreen(MyGame Handler, String map){
        this.Handler = Handler;

        batch = new SpriteBatch();
        camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.update();
        gameMap = new TiledGameMap(map);
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(135/255f, 206/255f, 235/255f, 1);
        Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);

        camera.position.set(gameMap.player.getX(), gameMap.player.getY(), 0);
        camera.update();
        gameMap.update(Gdx.graphics.getDeltaTime());
        gameMap.render(camera, batch);

        if (gameMap.gameStatus().equals("Game over")) {
            System.out.println("You died! Game over.");
            Handler.setScreen(new StartScreen(Handler));
        } else if (gameMap.gameStatus().equals("Stage complete")) {
            System.out.println("Stage complete! You found the golden boat and killed all the enemies");
            Handler.setScreen(new PlayScreen(Handler, extraMap));
        }
        if (!gameMap.player.isAlive()){
            Handler.setScreen(new DeathScreen(Handler));
        }
        if (gameMap.player.hasChest()){
            Handler.setScreen(new WinScreen(Handler));

        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        batch.dispose();
        gameMap.dispose();

    }
}
