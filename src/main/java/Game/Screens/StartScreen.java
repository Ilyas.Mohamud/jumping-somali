package Game.Screens;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;


public class StartScreen implements Screen {

    Texture PlayOn;
    Texture PlayOff;
    Texture QuitOn;
    Texture QuitOff;
    Texture Background;
    public PlayerCamera camera;

    final MyGame handler;

    private static final int ExitWidth = 250;
    private static final int ExitHeight = 120;
    private static final int PlayWidth = 300;
    private static final int PlayHeight = 120;
    private static final int Exit_Y = 100;
    private static final int Play_Y = 230;
    private static final int LOGO_Y = 400;
    private static final int LOGO_HEIGHT = 500;
    private static final int LOGO_WIDTH = 400;
    private String defaultMap = "FirstMap.tmx";

    /**
     * Constructor for StartScreen
     * Uses method touchdown to check if either Play or Exit is pressed
     * @param handler - The ScreenHandler
     */
    public StartScreen(final MyGame handler) {
        this.handler = handler;
        PlayOn = new Texture("PLAY.png");
        PlayOff = new Texture("PLAYInactive.png");
        QuitOn = new Texture("Quit.png");
        QuitOff = new Texture("QuitInactive.png");
        Background = new Texture("GTASomaliaBackground.jpg");

        final StartScreen startScreen = this;

        Gdx.input.setInputProcessor(new InputAdapter() {

            @Override
            public boolean touchDown(int screenX, int screenY, int pointer, int button) {
                //Exit button
                int x = MyGame.WIDTH / 2 - ExitWidth / 2;
                if (handler.camera.getScreenInput().x < x + ExitWidth && handler.camera.getScreenInput().x > x && MyGame.HEIGHT - handler.camera.getScreenInput().y < Exit_Y + ExitHeight && handler.HEIGHT - handler.camera.getScreenInput().y > Exit_Y) {
                    startScreen.dispose();
                    Gdx.app.exit();
                }

                //Play game button
                x = handler.WIDTH / 2 - PlayWidth / 2;
                if (handler.camera.getScreenInput().x < x + PlayWidth && handler.camera.getScreenInput().x > x && handler.HEIGHT - handler.camera.getScreenInput().y < Play_Y + PlayHeight && handler.HEIGHT - handler.camera.getScreenInput().y > Play_Y) {
                    startScreen.dispose();
                    handler.setScreen(new PlayScreen(handler, defaultMap));
                }

                return super.touchUp(screenX, screenY, pointer, button);
            }

        });
    }





    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {

        Gdx.gl.glClearColor(0, 0, 0, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        handler.batch.begin();

        int x = handler.WIDTH / 2 - ExitWidth / 2;
        if (handler.camera.getScreenInput().x < x + ExitWidth && handler.camera.getScreenInput().x > x && handler.HEIGHT - handler.camera.getScreenInput().y < Exit_Y + ExitHeight && handler.HEIGHT - handler.camera.getScreenInput().y > Exit_Y) {
            handler.batch.draw(QuitOn, x, Exit_Y, ExitWidth, ExitHeight);
        } else {
            handler.batch.draw(QuitOff, x, Exit_Y, ExitWidth, ExitHeight);
        }

        x = handler.WIDTH / 2 - PlayWidth / 2;
        if (handler.camera.getScreenInput().x < x + PlayWidth && handler.camera.getScreenInput().x > x && handler.HEIGHT - handler.camera.getScreenInput().y < Play_Y + PlayHeight && handler.HEIGHT - handler.camera.getScreenInput().y > Play_Y) {
            handler.batch.draw(PlayOn, x, Play_Y, PlayWidth, PlayHeight);
        } else {
            handler.batch.draw(PlayOff, x, Play_Y, PlayWidth, PlayHeight);
        }


         handler.batch.draw(Background, handler.WIDTH / 2 - LOGO_WIDTH / 2, LOGO_Y, LOGO_WIDTH, LOGO_HEIGHT);



        handler.batch.end();
    }




    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        Gdx.input.setInputProcessor(null);

    }
}
