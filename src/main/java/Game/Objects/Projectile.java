package Game.Objects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import Game.Map.GameMap;

import java.util.ArrayList;

public class Projectile {

    private float range;
    private int damage;
    private float speed;
    private Texture projectile;
    private Vector2 position;
    private float startX;
    private boolean exist;
    private GameMap map;
    private ItemType type;

    /**
     * Initiates another projectile
     * @param x - x-position where the projectile is fired from
     * @param y - y-position where the projectile is fired from
     * @param range - The maximum range the projectile can travel
     * @param damage - The damage the projectile inflicts
     * @param speed - Speed of the projectile
     * @param projectile - Texture of the projectile
     * @param map - The map where the projectile is fired
     * @param type - Which itemType the projectile belongs to
     */
    public Projectile(float x, float y, float range, int damage, float speed, Texture projectile, GameMap map, ItemType type) {
        this.position = new Vector2(x, y);
        this.range = range;
        this.damage = damage;
        this.speed = speed;
        this.projectile = projectile;
        this.startX = x;
        this.map = map;
        this.type = type;
        this.exist = true;
    }

    /**
     * Updates the position of the projectile. It cease to exist if it crashes in anything or move to maximum range.
     * @param enemies - Array of enemies located on the map
     * @param delta - DeltaTime: time since last update.
     */
    public void update(ArrayList<Enemy> enemies, float delta) {
        //A projectile cease t exist if it moves out of its initial range
        if ((range + startX) - (position.x+(speed*delta)) <= 0) {
            exist = false;
            return;
        }
        //A projectile cease to exist if it hits an enemy
        for (Enemy enemy: enemies) {
            if (collides(enemy, delta)) {
                enemy.takesDamage(damage);
                exist = false;
                return;
            }
        }
        //Projectile moves further if it doesn't collide with the map
        if (!map.rectCollidesWithMap(position.x+(speed*delta), position.y, type.getWidth(), type.getHeight())) {
            position.x = position.x + (speed*delta);
        }
        //A projectile cease to exist if it collides with the map
        else {
            exist = false;
        }
    }

    /**
     * Renders the projectile to the screen if it still exist
     * @param batch - Batch to draw the texture to the screen
     */
    public void render(Batch batch) {
        if (exist){
            batch.draw(projectile, position.x, position.y, type.getWidth(), type.getHeight());
        }

    }

    /**
     * Calculates if the projectile collides with a given enemy.
     * @param enemy - The enemy we want to compare the projectiles position to
     * @param delta - DeltaTime: Time since last update(used to measure the new x-value)
     * @return true if the projectile collides, false otherwise
     */
    public boolean collides(Enemy enemy, float delta) {
        float enemyXLeft = enemy.getX();
        float enemyXRight = enemy.getX() + enemy.getWidth();


        return ((position.x + (speed*delta) >= enemyXLeft)
                && position.x + (speed*delta) <= enemyXRight) || //projectile ends on an enemy
                (position.x < enemyXLeft
                        && position.x + (speed*delta) > enemyXRight); //projectile moves through an enemy
    }

    /**
     *
     * @return true if the projectile still exist, false otherwise
     */
    public boolean exists() {
        return exist;
    }
}
