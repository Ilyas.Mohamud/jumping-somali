package Game.Objects;

import  com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import Game.Map.GameMap;

/**
 * Representation of the Enemy in the game. This class decides all information and Textures for Enemy,
 * and controls Enemy movement.
 * It is responsible for correct updating and rendering of the Enemy object.
 */

public class Enemy extends Entity {

    private static final int SPEED = 140;
    private static final int HEALTH = 50;
    Texture image;

    private float startPosX;
    private float walkRange = 100f;
    private boolean shouldMoveRight = true;

    /**
     * Constructs an entity with the several parameters
     *
     * @param x
     * @param y
     * @param map
     */
    public Enemy(float x, float y, GameMap map) {
        super(x, y, EntityType.ENEMY, map);
        name = "Enemy";
        image = new Texture("Skeleton.png");
        hp = HEALTH;
        damage = 20;
        startPosX = x;
    }

    @Override
    public void render(SpriteBatch batch) {
        if (hp > 0){
            batch.draw(image, position.x, position.y, getWidth(),getHeight());
        }

    }

    /**
     * Extention of update method in Entity.
     * Enemy moves back and forth between startPosX and startPosX + walkRange
     *
     * @param deltaTime - used by gdx to synchronize events.
     * @param gravity - gravity constant to represent the gravitational force.
     */
    public void update(float deltaTime, float gravity) {
        super.update(deltaTime,gravity);

        if(grounded) {
            if(shouldMoveRight) {
                moveX(SPEED * deltaTime);
            } else {
                moveX(-SPEED * deltaTime);
            }
        }

        if(this.getX() > startPosX + walkRange) {
            shouldMoveRight = false;
        } else if(this.getX() < startPosX) {
            shouldMoveRight = true;
        }

    }

}
