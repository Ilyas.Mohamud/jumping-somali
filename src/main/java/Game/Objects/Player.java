package Game.Objects;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import Game.Map.GameMap;

import java.util.ArrayList;

/**
 * Representation of the Player in the game. This class decides all information and Textures for Player,
 * and controls Player movement and damage.
 * It is responsible for correct updating and rendering of the Player object.
 */
public class Player extends Entity {

    private static final int SPEED = 200;
    private static final int JUMP_VELOCITY = 4;
    private static final int HEALTH = 100;
    private Texture healthBar;
    Texture image;
    final float DMG_INTERVAL = .5f;
    float dmg_time = 0;
    private String facesDir;
    private int coins;
    public ArrayList<Item> inventory;
    private Weapon weapon;

    public Weapon getCurrentWeapon() {
        if(weapon != null)
            return weapon;
        else
            return null;
    }

    public Player (float x, float y, GameMap map) {
        super(x, y, EntityType.PLAYER, map);
        name = "Player";
        image = new Texture("pirateplayer.png");
        hp = HEALTH;
        damage = 15;
        healthBar = new Texture("healthbar.png");
        coins = 0;
        inventory = new ArrayList<>();
        weapon = null;

    }

    /**
     * Extention of update method in Entity.
     * Player has its own set of rules for movement on the map, based on
     * keyboard input.
     *
     * @param deltaTime - used by gdx to synchronize events.
     * @param gravity - gravity constant to represent the gravitational force.
     */
    public void update(float deltaTime, float gravity) {

        if (Gdx.input.isKeyPressed(Input.Keys.SPACE) && grounded) {
            this.velocityY += JUMP_VELOCITY * getWeight();
        }
        else if (Gdx.input.isKeyPressed(Input.Keys.SPACE) && !grounded && this.velocityY > 0) {
            this.velocityY += JUMP_VELOCITY * getWeight()* deltaTime;
        }

        super.update(deltaTime,gravity);

        if (Gdx.input.isKeyPressed(Input.Keys.A)) {
            moveX(-SPEED * deltaTime);
        }

        if (Gdx.input.isKeyPressed(Input.Keys.D)) {
            moveX(SPEED * deltaTime);
        }

        // For each DMG_INTERVAL Player is colliding with an Enemy object, Player takes damage.
        dmg_time += Gdx.graphics.getDeltaTime();
        if (dmg_time > DMG_INTERVAL) {
            collidesWithEnemy();
            dmg_time -= DMG_INTERVAL;
        }

        if (Gdx.input.isKeyPressed(Input.Keys.F)){
            if (weapon != null){
                weapon.fire(this, map);
            }
        }
    }


    @Override
    public void render(SpriteBatch batch) {
        drawHealthBar(batch);
        batch.draw(image, position.x, position.y, getWidth(),getHeight());
        if (hp > 60)
            batch.setColor(Color.GREEN);
        else if (hp > 30)
            batch.setColor(Color.ORANGE);
        else
            batch.setColor(Color.RED);

        batch.draw(healthBar, position.x-900, position.y+450,  hp*4, 15);

        batch.setColor(Color.WHITE);
    }

    /**
     * Draws the Player health bar, relative to Player position on the map.
     *
     * @param batch - the Player SpriteBatch to draw the health bar on.
     */
    public void drawHealthBar(SpriteBatch batch) {
        batch.setColor(Color.BLACK);
        batch.draw(healthBar, position.x - 900-5, position.y+450-5, hp * 4+10, 15+10);
        batch.setColor(Color.WHITE);

    }

    /**
     * Loops through Enemy objects on the map, checking if CollisionArea of Player
     * collides with CollisionArea of any Enemy. Player takes damage if they collide.
     *
     * @return true if collision with Enemy is detected, false otherwise.
     */
    public boolean collidesWithEnemy() {
        ArrayList<Entity> entities = map.getEntities();
        for (Entity entity : entities) {
            if (entity.getName().equals("Enemy") && collisionArea.collidesWith(entity.getCollisionArea())) {
                takesDamage(entity.damage);
                return true;
            }
        }
        return false;
    }


    /**
     * Adds a given amount to the total coin amount
     * @param amount -Amount of coins given
     */
    public void addCoins(int amount) {
        coins += amount;
        System.out.println("Total coins: " + coins);
    }




    /**
     * Equips the player with a Weapon
     * @param w - The Weapon equipped
     */
    public void equip(Weapon w) {
        if (weapon != null){
            weapon.putDown(this);
        }
        this.weapon = w;
        inventory.add(w);
    }


    public boolean hasBoat() {
        for (Item item : inventory) {
            if (item.name.equals("GOLDBOAT")) {
                return true;
            }
        }
        return false;
    }

    public boolean hasChest() {
        for (Item item : inventory) {
            if (item.name.equals("TREASURECHEST")) {
                return true;
            }
        }
        return false;
    }
}

