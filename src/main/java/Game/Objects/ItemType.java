package Game.Objects;

import Game.Map.TileType;
import com.badlogic.gdx.graphics.Texture;


public enum ItemType {


    TREASURECHEST(TileType.TILE_SIZE, TileType.TILE_SIZE, 10000, "TREASURECHEST", new Texture("TreasureChest.png"), "TREASURE"),
    GOLDBOAT(TileType.TILE_SIZE *3, TileType.TILE_SIZE*3, 500, "GOLD BOAT", new Texture("GoldBoat.png"), "TREASURE"),
    BULLET(TileType.TILE_SIZE , TileType.TILE_SIZE*2, 0, "BULLET", new Texture("Bullet.png"), "WEAPON");


    private int height;
    private int width;
    private int value;
    private String name;
    private Texture texture;
    private String instance;

    /**
     * Initiates a new instance of ItemType
     * @param height - Height of the item
     * @param width - Width if the item
     * @param value - Coin-value of the item
     * @param name - Name of the item
     * @param texture - The texture of the item
     * @param instance - Which class the item is a instance of
     */
    ItemType(int height, int width, int value, String name, Texture texture, String instance){
        this.height = height;
        this.width = width;
        this.value = value;
        this.name = name;
        this.texture = texture;
        this.instance = instance;
    }

    public int getWidth() {return width;}

    public int getHeight() {return height;}

    public int getValue() {return value;}

    public Texture getTexture() {return texture;}

    public String getInstance() {return instance;}

}
