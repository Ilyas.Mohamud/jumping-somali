package Game.Objects;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import Game.Map.CollisionArea;

public abstract class Item {
    protected Vector2 position;
    protected ItemType type;
    protected boolean isPickedUp;
    protected CollisionArea collisionArea;
    protected long timeSincePutDown;
    protected String name;

    /**
     * Creates another item instance.
     * @param pos - Position of the item in question
     * @param t - Type of the item
     */
    public Item(Vector2 pos, ItemType t) {
        this.position = pos;
        this.type = t;
        this.name = t.name();
        this.isPickedUp = false;
        this.collisionArea = new CollisionArea(pos.x, pos.y, t.getWidth(), t.getHeight());
        timeSincePutDown = System.currentTimeMillis();
    }


    /**
     * Calculates if a given player collides with the item, which results in a pickup of the item
     * @param player
     * @param delta
     */
    public void update(Player player, float delta){
        //Pick up if a player collides and it is at least 1 sec since the item was put down.
        if (!isPickedUp && player.getCollisionArea().collidesWith(collisionArea) && System.currentTimeMillis() - timeSincePutDown > 1000){
            isPickedUp = true;
            pickUp(player);
        }
    }

    /**
     * Method which decides what happens with the item when it is picked up
     * @param player - The player picking the item up
     */
    public abstract void pickUp(Player player);


    /**
     * Puts a Weapon back on the ground
     * @param player - Player which puts the Weapon down
     */
    public void putDown(Player player) {
        position.x = player.getX();
        position.y = player.getY();
        collisionArea.move(position.x, position.y);
        isPickedUp = false;
    }

    /**
     * Renders a given item to the screen
     * @param batch - Batch which writes the texture to the screen
     */
    public abstract void render(Batch batch);


}
