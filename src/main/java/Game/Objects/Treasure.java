package Game.Objects;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;

public class Treasure extends Item {

    private boolean isCollected;

    /**
     * Initiates a new Treasure
     * @param x - x-position where the item is located on the map
     * @param y - y-position where the item is located on the map
     * @param t - ItemType of the Treasure
     */
    public Treasure(float x, float y, ItemType t) {
        super(new Vector2(x, y), t);
        isCollected = false;
    }

    /**
     * Updates the Treasure by calling the super.update method
     * @param player - The player of the game
     * @param delta - DeltaTime: Time since last update
     */
    @Override
    public void update(Player player, float delta) {
        super.update(player, delta);
    }

    /**
     * Makes a player pick up the item by adding the item to inventory and
     * giving it coins.
     * @param player - The player picking the item up
     */
    @Override
    public void pickUp(Player player) {
        if (!isCollected){
            player.addCoins(type.getValue());
            player.inventory.add(this);
            player.hasBoat();
            isCollected = true;
        }

    }


    @Override
    public void render(Batch batch) {
        if (!isPickedUp) {
            batch.draw(type.getTexture(), position.x, position.y, type.getWidth(), type.getHeight());
        }
    }
}

