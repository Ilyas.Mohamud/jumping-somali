package Game;

import static org.junit.jupiter.api.Assertions.*;

import Game.Map.GameMap;
import Game.Map.TiledGameMap;
import Game.Objects.Player;
import Game.Objects.Weapon;
import Game.Screens.MyGame;
import Game.Screens.PlayScreen;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class AppTest {
	private GameMap gameMap;
	float deltaX, deltaY;
	MyGame Handler;


	
	
	/**
	 * Static method run before everything else
	 */
	@BeforeAll
	static void setUpBeforeAll() {
		/**Lwjgl3ApplicationConfiguration cfg = new Lwjgl3ApplicationConfiguration();
		cfg.setTitle("Jumping-Somali");
		cfg.setWindowedMode(1920, 1080);
		new Lwjgl3Application(new MyGame(), cfg);

		*/

	}

	/**
	 * Setup method called before each of the test methods
	 */
	@BeforeEach
	void setUpBeforeEach() {
	}

	/**
	 * Simple test case
	 */
	@Test
	void playerDies() {

	}

	/**
	 * Simple test case
	 */
	@Test
	void dummy2() {
		// For floats and doubles it's best to use assertEquals with a delta, since
		// floating-point numbers are imprecise
		float a = 100000;
		a = a + 0.1f;
		assertEquals(100000.1, a, 0.01);
	}

	/**
	 * Parameterized test case, reading arguments from comma-separated strings
	 * 
	 * @param a
	 * @param b
	 * @param c
	 */
	@CsvSource(value = { "1,1,2", "1,2,3", "2,3,5", "3,5,8", "5,8,13", "8,13,21" })
	@ParameterizedTest(name = "{0}+{1} == {2}")
	void addTest(int a, int b, int c) {
		assertEquals(c, a + b);
	}
}